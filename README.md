# README #

### About This Repository ###
  
* Some of the first Swift projects that I worked on, during the summer of 2015, based off tutorials followed online. I spent the majority of the summer learning iOS development with Swift culminating in making my own app from scratch that's now live in the App Store.  
  
* **NOTE:** These projects are out of date. They are neither converted to the latest Swift syntax (coded in Swift 1.2), nor updated to Xcode's recommended updated settings –– they were good starting points for learning, but I no longer keep them updated as I've moved on to new projects.  
