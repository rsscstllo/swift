//
//  LoginViewController.swift
//  LoginProject
//
//  Created by Ross Castillo on 6/23/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

  @IBOutlet weak var usernameTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // Set the delegate equal to self here.
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "loginToCreateAccountSegue" {
      let createAccountVC = segue.destinationViewController as! CreateAccountViewController
      createAccountVC.delegate = self
    }
  }
  
  // Access information stored in NSUD
  @IBAction func loginButtonPressed(sender: UIButton) {
    let usernameSavedFromNSUserDefaults = NSUserDefaults.standardUserDefaults().objectForKey(kUserNameKey) as! String
    print(usernameSavedFromNSUserDefaults)
    
    let passwordSavedFromNSUserDefaults = NSUserDefaults.standardUserDefaults().objectForKey(kPasswordKey) as! String
    print(passwordSavedFromNSUserDefaults)
    
    if usernameTextField.text == usernameSavedFromNSUserDefaults && passwordTextField.text == passwordSavedFromNSUserDefaults {
      self.performSegueWithIdentifier("loginToMainSegue", sender: self)
    }
  }
  
  @IBAction func createAccountButtonPressed(sender: UIButton) {
    self.performSegueWithIdentifier("loginToCreateAccountSegue", sender: self)
  }
  
}

extension LoginViewController: CreateAccountViewControllerDelegate {
  // When the account is created, skip this screen after showing it for a second, and go to the main screen. 
  // A better solution would be to not even go back to this screen, but this gives practice with delegates.
  func accountCreated() {
    self.performSegueWithIdentifier("loginToMainSegue", sender: nil)
  }
}
