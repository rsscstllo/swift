//
//  CreateAccountViewController.swift
//  LoginProject
//
//  Created by Ross Castillo on 6/23/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit

// A protocol is an extension to our CreateAccountViewController, that will allow us to implement our 
// functions, in this case, accountCreated(), in another class (LoginViewController class).
protocol CreateAccountViewControllerDelegate {
  func accountCreated ()
}

class CreateAccountViewController: UIViewController {
  
  var delegate: CreateAccountViewControllerDelegate?

  @IBOutlet weak var chooseUsernameTextField: UITextField!
  @IBOutlet weak var choosePasswordTextField: UITextField!
  @IBOutlet weak var confirmPasswordTextField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func createAccountButtonPressed(sender: UIButton) {
    if choosePasswordTextField.text == confirmPasswordTextField.text && choosePasswordTextField.text!.isEmpty != true {
      NSUserDefaults.standardUserDefaults().setObject(self.chooseUsernameTextField.text, forKey: kUserNameKey)
      NSUserDefaults.standardUserDefaults().setObject(self.choosePasswordTextField.text, forKey: kPasswordKey)
      // Save to NSUD
      NSUserDefaults.standardUserDefaults().synchronize()
      self.dismissViewControllerAnimated(true, completion: nil)
      delegate?.accountCreated()
    }
  }
  
  @IBAction func cancelButtonPressed(sender: UIButton) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }

}
