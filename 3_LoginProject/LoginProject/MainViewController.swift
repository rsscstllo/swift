//
//  MainViewController.swift
//  LoginProject
//
//  Created by Ross Castillo on 6/23/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

  @IBOutlet weak var usernameLabel: UILabel!
  @IBOutlet weak var passwordLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    // When accessing information from NSUD, there's no guarantee that anything will actually come back, 
    // so "as" needs to be an optional. For example, if nothing were saved yet, this would return nil.
    usernameLabel.text = NSUserDefaults.standardUserDefaults().objectForKey(kUserNameKey) as? String
    passwordLabel.text = NSUserDefaults.standardUserDefaults().objectForKey(kPasswordKey) as? String
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

}
