//
//  ViewController.swift
//  YAYorNAY
//
//  Created by Ross Castillo on 7/8/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    // Testing CardView.swift
    self.view.addSubview(CardView(frame: CGRectMake(80.0, 80.0, 120.0, 200.0)))
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}
