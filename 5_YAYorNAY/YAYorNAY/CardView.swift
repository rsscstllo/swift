//
//  CardView.swift
//  YAYorNAY
//
//  Created by Ross Castillo on 7/8/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import Foundation
import UIKit

class CardView: UIView {
  
  // In-line initialization so it already has a value when it's created.
  private let imageView: UIImageView = UIImageView()
  private let nameLabel: UILabel = UILabel()
  
  // Necessary "required" since overriding the initialization of an Apple class (UIView).
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    initialize()
  }
  
  // Need to override the other initializers for UIView() since other classes could call it differently.
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initialize()
  }
  
  init() {
    super.init(frame: CGRectZero)
    initialize()
  }
  
  // Set up the imageView, nameLabel, and borders of the view.
  private func initialize() {
    imageView.translatesAutoresizingMaskIntoConstraints = false  // Will be using custom contraints.
    imageView.backgroundColor = UIColor.redColor()
    addSubview(imageView)
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    addSubview(nameLabel)
    
    backgroundColor = UIColor.whiteColor()
    layer.borderWidth = 0.5
    layer.borderColor = UIColor.lightGrayColor().CGColor
    layer.cornerRadius = 5
    layer.masksToBounds = true  // Make sure subview doesn't exceed bounds of its super view.
    
    setConstraints()
  }
  
  // Set up the x, y, width and height constraints for the imageView and nameLabel.
  private func setConstraints() {
    // imageView
    addConstraint(NSLayoutConstraint(item: imageView,
                                attribute: NSLayoutAttribute.Top,
                                relatedBy: NSLayoutRelation.Equal,
                                   toItem: self,
                                attribute: NSLayoutAttribute.Top,
                               multiplier: 1.0,
                                 constant: 0))
    
    addConstraint(NSLayoutConstraint(item: imageView,
                                attribute: NSLayoutAttribute.Leading,
                                relatedBy: NSLayoutRelation.Equal,
                                   toItem: self,
                                attribute: NSLayoutAttribute.Leading,
                               multiplier: 1.0,
                                 constant: 0))
    
    addConstraint(NSLayoutConstraint(item: imageView,
                                attribute: NSLayoutAttribute.Width,
                                relatedBy: NSLayoutRelation.Equal,
                                   toItem: self,
                                attribute: NSLayoutAttribute.Width,
                               multiplier: 1.0,
                                 constant: 0))
    
    addConstraint(NSLayoutConstraint(item: imageView,
                                attribute: NSLayoutAttribute.Height,
                                relatedBy: NSLayoutRelation.Equal,
                                   toItem: self,
                                attribute: NSLayoutAttribute.Height,
                               multiplier: 1.0,
                                 constant: 0))
    
    // nameLabel
    addConstraint(NSLayoutConstraint(item: nameLabel,
                                attribute: NSLayoutAttribute.Top,
                                relatedBy: NSLayoutRelation.Equal,
                                   toItem: imageView,
                                attribute: NSLayoutAttribute.Bottom,
                               multiplier: 1.0,
                                 constant: 0))
    
    addConstraint(NSLayoutConstraint(item: nameLabel,
                                attribute: NSLayoutAttribute.Leading,
                                relatedBy: NSLayoutRelation.Equal,
                                   toItem: self,
                                attribute: NSLayoutAttribute.Leading,
                               multiplier: 1.0,
                                 constant: 10))
    
    addConstraint(NSLayoutConstraint(item: nameLabel,
                                attribute: NSLayoutAttribute.Trailing,
                                relatedBy: NSLayoutRelation.Equal,
                                   toItem: self,
                                attribute: NSLayoutAttribute.Trailing,
                               multiplier: 1.0,
                                 constant: -10))
    
    addConstraint(NSLayoutConstraint(item: nameLabel,
                                attribute: NSLayoutAttribute.Bottom,
                                relatedBy: NSLayoutRelation.Equal,
                                   toItem: self,
                                attribute: NSLayoutAttribute.Bottom,
                               multiplier: 1.0,
                                 constant: 0))
  }
  
}
