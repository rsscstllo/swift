//
//  CardsViewController.swift
//  YAYorNAY
//
//  Created by Ross Castillo on 7/8/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit

class CardsViewController: UIViewController {
  
  struct Card {
    let cardView: CardView
    let swipeView: SwipeView
  }
  
  let frontCardTopMargin: CGFloat = 0
  let backCardTopMargin: CGFloat = 10
  
  @IBOutlet weak var cardStackView: UIView!
  
  var backCard: Card?  // Can have a value and can also be nil.
  var frontCard: Card?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    // Set up back card.
    backCard = createCard(backCardTopMargin)
    cardStackView.addSubview(backCard!.swipeView)
    
    // Set up front card.
    frontCard = createCard(frontCardTopMargin)
    cardStackView.addSubview(frontCard!.swipeView)
    
    // No longer need to set delgates since they're set in the createCard function.
//    // Set the delegate properties.
//    backCard!.delegate = self
//    frontCard!.delegate = self
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Helper Functions
  
  // Create a frame for the cards.
  private func createCardFrame(topMargin: CGFloat) -> CGRect {
    return CGRect(x: 0, y: topMargin, width: cardStackView.frame.width, height: cardStackView.frame.height)
  }
  
  private func createCard(topMargin: CGFloat) -> Card {
    // Create an instance of CardView().
    let cardView = CardView()
    // Create an instance of SwipeView() via the second constructor.
    let swipeView = SwipeView(frame: createCardFrame(topMargin))
    swipeView.delegate = self
    swipeView.innerView = cardView
    return Card(cardView: cardView, swipeView: swipeView)
  }
  
}

// MARK: - SwipeViewDelegate

extension CardsViewController: SwipeViewDelegate {
  func swipedLeft() {
    print("left")
    // If frontCard has a value, assign it to "let frontCard".
    if let frontCard = frontCard {
      frontCard.swipeView.removeFromSuperview()
    }
  }
  func swipedRight() {
    print("right")
    if let frontCard = frontCard {
      frontCard.swipeView.removeFromSuperview()
    }
  }
}