//
//  SwipeView.swift
//  YAYorNAY
//
//  Created by Ross Castillo on 7/8/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import Foundation
import UIKit

protocol SwipeViewDelegate: class {
  func swipedLeft()
  func swipedRight()
}

class SwipeView: UIView {
  
  enum Direction {
    case None
    case Left
    case Right
  }
  
  weak var delegate: SwipeViewDelegate?  // weak prevents a retain cycle (more memory efficient).
  
//  // Gives access to an instance of CardView().
//  private let card: CardView = CardView()
  
  var innerView: UIView? {
    didSet {
      if let v = innerView {
        addSubview(v)
        v.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
      }
    }
  }
  
  // Store the original location of the card to calculate its distance moved.
  private var originalPoint: CGPoint?
  
  // Override the designated initializers for UIView().
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
    initialize()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initialize()
  }
  
  init() {
    super.init(frame: CGRectZero)
    initialize()
  }
  
  // Custom Initialization.
  
  private func initialize() {
    // TODO: Change back to clear
    self.backgroundColor = UIColor.redColor()  // Swipe should be clear.
//    addSubview(card)
    
    self.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: "dragged:"))
    
//    card.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
    
    // Removed AutoLayout
//    card.setTranslatesAutoresizingMaskIntoConstraints(false)
//    setConstraints()
  }
  
  // This function gets called over and over again while being dragged.
  func dragged(gestureRecognizer: UIPanGestureRecognizer) {
    let distance = gestureRecognizer.translationInView(self)  // self, referring to the SwipeView.
    print("Distance x:\(distance.x) y: \(distance.y)")
    
    switch gestureRecognizer.state {
    case UIGestureRecognizerState.Began:
      originalPoint = center
    case UIGestureRecognizerState.Changed:
      
      // min compares its first parameter to 1 and returns it if it's less than 1, and returns 1 otherwise.
      let rotationPercent = min(distance.x/(self.superview!.frame.width/2), 1)
      let rotationAngle = (CGFloat(2*M_PI/16)*rotationPercent)
      
      transform = CGAffineTransformMakeRotation(rotationAngle)
      
      // Everytime a card is dragged, the center of the swipe view is changed. This takes the original point
      // and adds the distance/difference for x and y to move the view's center.
      center = CGPointMake(originalPoint!.x + distance.x, originalPoint!.y + distance.y)
      
    case UIGestureRecognizerState.Ended:
      if abs(distance.x) < frame.width/4 {
        resetViewPositionAndTransformations()
      }
      else {
        swipe(distance.x > 0 ? .Right : .Left)  // Ternary Operator
      }
      // resetViewPositionAndTransformations()
    default:
      print("Default trigged for GestureRecognizer")  // Would print if something went wrong.
      break
    }
  }
  
  private func resetViewPositionAndTransformations() {
    UIView.animateWithDuration(0.2, animations: { () -> Void in
      self.center = self.originalPoint!
      self.transform = CGAffineTransformMakeRotation(0)
    })
  }
  
  // Direction is the enumerated type.
  func swipe(s: Direction) {
    if s == .None {  // Could write as Direction.None but .None is valid since the enum is inside this class.
      return
    }
    
    var parentWidth = superview!.frame.size.width
    
    if s == .Left {
      parentWidth *= -1
    }
    
    UIView.animateWithDuration(0.2, animations: {
      self.center.x = self.frame.origin.x + parentWidth
      }, completion: {
        // completion expects a boolean (success), if true run the next line.
        success in
        if let d = self.delegate { // if the delegate optional exists, assign its value to d.
          s == .Right ? d.swipedRight() : d.swipedLeft()
        }
    })
    
//    UIView.animateWithDuration(0.2, animations: { () -> Void in
//      self.center.x = self.frame.origin.x + parentWidth
//    })
  }
  
  // AutoLayout and transforms don't work well together.
//  // Programmatically equivalent to dragging up, down, left, and right in storyboard via autolayout.
//  private func setConstraints() {
//    addConstraint(NSLayoutConstraint(item: card,
//                                attribute: NSLayoutAttribute.Top,
//                                relatedBy: NSLayoutRelation.Equal,
//                                   toItem: self,
//                                attribute: NSLayoutAttribute.Top,
//                               multiplier: 1.0,
//                                 constant: 0))
//    
//    addConstraint(NSLayoutConstraint(item: card,
//                                attribute: NSLayoutAttribute.Bottom,
//                                relatedBy: NSLayoutRelation.Equal,
//                                   toItem: self,
//                                attribute: NSLayoutAttribute.Bottom,
//                               multiplier: 1.0,
//                                 constant: 0))
//    
//    addConstraint(NSLayoutConstraint(item: card,
//                                attribute: NSLayoutAttribute.Leading,
//                                relatedBy: NSLayoutRelation.Equal,
//                                   toItem: self,
//                                attribute: NSLayoutAttribute.Leading,
//                               multiplier: 1.0,
//                                 constant: 0))
//    
//    addConstraint(NSLayoutConstraint(item: card,
//                                attribute: NSLayoutAttribute.Trailing,
//                                relatedBy: NSLayoutRelation.Equal,
//                                   toItem: self,
//                                attribute: NSLayoutAttribute.Trailing,
//                               multiplier: 1.0,
//                                 constant: 0))
//  }
  
}
