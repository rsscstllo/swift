//
//  FilterViewController.swift
//  ExchangeAGram
//
//  Created by Ross Castillo on 6/8/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {
  
  var thisFeedItem: FeedItem!  // Going to pass in a FeedItem to display it with filters
  var collectionView: UICollectionView!  // To setup a collectionview programmitically
  let kIntensity = 0.7
  var context: CIContext = CIContext(options: nil)
  var filters: [CIFilter] = []
  let placeHolderImage = UIImage(named: "Placeholder")
  let tmp = NSTemporaryDirectory() // returns path of temporary directory for the current user
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    // Set up collection view
    let layout = UICollectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    layout.itemSize = CGSize(width: 150.0, height: 150.0)
    // Setup with a cusstom initializer
    collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
    
    collectionView.dataSource = self
    collectionView.delegate = self
    
    collectionView.backgroundColor = UIColor.whiteColor()
    // Obj-c would be [FilterCell class], but swift accesses class with FilterCell.self
    collectionView.registerClass(FilterCell.self, forCellWithReuseIdentifier: "MyCell")
    self.view.addSubview(collectionView)
    
    filters = photoFilters()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // Helper
  
  func photoFilters () -> [CIFilter] {
    let blur       = CIFilter(name: "CIGaussianBlur")
    let instant    = CIFilter(name: "CIPhotoEffectInstant")
    let noir       = CIFilter(name: "CIPhotoEffectNoir")
    let transfer   = CIFilter(name: "CIPhotoEffectTransfer")
    let unsharpen  = CIFilter(name: "CIUnsharpMask")
    let monochrome = CIFilter(name: "CIColorMonochrome")
    
    let colorControls = CIFilter(name: "CIColorControls")
    colorControls.setValue(0.5, forKey: kCIInputSaturationKey)
    
    let sepia = CIFilter(name: "CISepiaTone")
    sepia.setValue(kIntensity, forKey: kCIInputIntensityKey)
    
    let colorClamp = CIFilter(name: "CIColorClamp")
    colorClamp.setValue(CIVector(x: 0.9, y: 0.9, z: 0.9, w: 0.9), forKey: "inputMaxComponents")
    colorClamp.setValue(CIVector(x: 0.2, y: 0.2, z: 0.2, w: 0.2), forKey: "inputMinComponents")
    
    let composite = CIFilter(name: "CIHardLightBlendMode")
    composite.setValue(sepia.outputImage, forKey: kCIInputImageKey)
    
    let vignette = CIFilter(name: "CIVignette")
    vignette.setValue(composite.outputImage, forKey: kCIInputImageKey)
    vignette.setValue(kIntensity * 2, forKey: kCIInputIntensityKey)
    vignette.setValue(kIntensity * 30, forKey: kCIInputRadiusKey)
    
    return [blur, instant, noir, transfer, unsharpen, monochrome, colorControls, sepia, colorClamp, composite, vignette]
  }
  
  func filteredImageFromImage (imageData: NSData, filter: CIFilter) -> UIImage {
    // convert data to image
    let unfilteredImage = CIImage(data: imageData)
    // pass in image to filter
    filter.setValue(unfilteredImage, forKey: kCIInputImageKey)
    let filteredImage: CIImage = filter.outputImage
    
    let extent = filteredImage.extent()  // determine what the rect of the image is - formatting for collection view
    let cgImage: CGImageRef = context.createCGImage(filteredImage, fromRect: extent)
    
    let finalImage = UIImage(CGImage: cgImage)  // convert to UIImage by passing in the cgImage
    
    return finalImage!
  }
  
  // MARK: - Caching functions
  
  func cacheImage(imageNumber: Int) {
    let fileName = "\(imageNumber)"
    let uniquePath = tmp.stringByAppendingPathComponent(fileName)
    // If it doesnt exist, generate an image with a filter
    if !NSFileManager.defaultManager().fileExistsAtPath(fileName) {
      let data = self.thisFeedItem.thumbNail
      let filter = self.filters[imageNumber]
      let image = filteredImageFromImage(data, filter: filter)  // generate an UIImage
      // "atomically" writes it to a back-up file
      UIImageJPEGRepresentation(image, 1.0).writeToFile(uniquePath, atomically: true)
    }
  }
  
  func getCachedImage (imageNumber: Int) -> UIImage {
    let fileName = "\(imageNumber)"
    let uniquePath = tmp.stringByAppendingPathComponent(fileName)
    var image: UIImage
    // Does the image exists? If not, use helper method above
    if NSFileManager.defaultManager().fileExistsAtPath(uniquePath) {
      image = UIImage(contentsOfFile: uniquePath)!  // have to unwrap this optional
    } else {
      self.cacheImage(imageNumber)
      image = UIImage(contentsOfFile: uniquePath)!
    }
    return image
  }
  
}

// MARK: - UICollectionViewDataSource

extension FilterViewController: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return filters.count
  }
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell:FilterCell = collectionView.dequeueReusableCellWithReuseIdentifier("MyCell", forIndexPath: indexPath) as! FilterCell
    
    // Dont need this if statement anymore with caching
    // if cell.imageView.image == nil {  // If statement so images don't reload after scrolling.
    
    // NOTE: hold down command key and left-bracket button to indent highlighted code to the left after removing if statements for example
    
    // cell.imageView.image = UIImage(named: "Placeholder")
    // Now, the same image is being used instead of making a new one every time.
    cell.imageView.image = placeHolderImage
    
    // GCD =====================================================================
    // Instead of running on another "thread", we just specify that it's running on another "queue"
    // Apple actually takes care of it running on another thread
    let filterQueue:dispatch_queue_t = dispatch_queue_create("filter queue", nil)
    
    dispatch_async(filterQueue, { () -> Void in
      // Have to use self keyword since inside of closure.
      // let filterImage = self.filteredImageFromImage(self.thisFeedItem.thumbNail, filter: self.filters[indexPath.row])
      let filterImage = self.getCachedImage(indexPath.row) // so file names aren't the same
      
      // Tell it to go back to the main thread
      dispatch_async(dispatch_get_main_queue(), { () -> Void in  // UI ALWAYS on the main thread
        cell.imageView.image = filterImage
      })
    })
    // GCD =====================================================================
    
    // Dont need this line anymore, this is now done in the background thread.
    // cell.imageView.image = filteredImageFromImage(thisFeedItem.image, filter: filters[indexPath.row])
    return cell
  }
}

// MARK: - UICollectionViewDelegate

extension FilterViewController: UICollectionViewDelegate {
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    // Create the filtered image.
    // use the full image instead of the thumbnail since this is what the filter should be applied to.
    let filterImage = self.filteredImageFromImage(self.thisFeedItem.image, filter: self.filters[indexPath.row])
    
    let imageData = UIImageJPEGRepresentation(filterImage, 1.0)
    // Update this feed item with the new image
    self.thisFeedItem.image = imageData
    
    // Update thumbnail
    let thumbNailData = UIImageJPEGRepresentation(filterImage, 0.1)
    self.thisFeedItem.thumbNail = thumbNailData
    
    // Persist to file system by accessing the app delegate and calling save on it
    (UIApplication.sharedApplication().delegate as! AppDelegate).saveContext()
    
    self.navigationController?.popViewControllerAnimated(true)
  }
}
