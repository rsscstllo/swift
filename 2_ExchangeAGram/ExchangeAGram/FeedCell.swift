//
//  FeedCell.swift
//  ExchangeAGram
//
//  Created by Ross Castillo on 6/7/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit

class FeedCell: UICollectionViewCell {
  
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var captionLabel: UILabel!
    
}
