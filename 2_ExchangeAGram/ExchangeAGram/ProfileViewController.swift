//
//  ProfileViewController.swift
//  ExchangeAGram
//
//  Created by Ross Castillo on 6/8/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

  // Weak storage type is recommended here since it will have a lot better memory management.
  // All connections should be weak unless you have a really good reason to declare them as strong.
  @IBOutlet weak var profileImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
