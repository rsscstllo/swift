//
//  FeedItem+CoreDataProperties.swift
//  ExchangeAGram
//
//  Created by Ross Castillo on 11/14/15.
//  Copyright © 2015 Ross Castillo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension FeedItem {

    @NSManaged var caption: String?
    @NSManaged var image: NSData?
    @NSManaged var thumbNail: NSData?

}
