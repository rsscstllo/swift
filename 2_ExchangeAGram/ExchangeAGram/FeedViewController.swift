//
//  FeedViewController.swift
//  ExchangeAGram
//
//  Created by Ross Castillo on 6/7/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit
import MobileCoreServices  // Gives access to UIImagePickerController, camera, and photo library
import CoreData  // allow creation of FeedItems and access to NSManagedContext from AppDelegate

class FeedViewController: UIViewController {
  
  var feedArray: [AnyObject] = []  // AnyObject to satisfy executeFetchRequest() return type
  @IBOutlet var collectionView: UICollectionView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    collectionView.dataSource = self
    collectionView.delegate = self
  }
  
  override func viewDidAppear(animated: Bool) {
    // Specify the entity name to get back all the FeedItems saved.
    let request = NSFetchRequest(entityName: "FeedItem")
    // Get an instance of the AppDelegate back.
    let appDelegate: AppDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    // Get access to context from the appDelegate instance.
    let context: NSManagedObjectContext = appDelegate.managedObjectContext!
    // Request will be returned as an array of FeedItems
    feedArray = context.executeFetchRequest(request, error: nil)!
    collectionView.reloadData()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - IBActions
  
  @IBAction func snapBarButtonItemTapped(sender: UIBarButtonItem) {
    // If the camera is available - this will only evaluate if deployed to a device.
    if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
      let cameraController = UIImagePickerController()
      cameraController.delegate = self
      cameraController.sourceType = UIImagePickerControllerSourceType.Camera
      // What type of data is the camera controller accessing?
      // kUTTypeImage == abstract type, typedef's to CFString - specifying that it will be image data.
      let mediaTypes: [AnyObject] = [kUTTypeImage]
      cameraController.mediaTypes = mediaTypes
      cameraController.allowsEditing = false
      // Once it's set up, now present the camera view controller on the screen.
      self.presentViewController(cameraController, animated: true, completion: nil)
    
    } else if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
      let photoLibraryController = UIImagePickerController()
      photoLibraryController.delegate = self  // Figure out which photo user selects from photo library.
      photoLibraryController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
      let mediaTypes:[AnyObject] = [kUTTypeImage]
      photoLibraryController.mediaTypes = mediaTypes
      photoLibraryController.allowsEditing = false
      self.presentViewController(photoLibraryController, animated: true, completion: nil)
      
    } else {
      let alertController = UIAlertController(title: "Alert", message: "Your device does not support the camera or photo Library", preferredStyle: UIAlertControllerStyle.Alert)
      alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
      self.presentViewController(alertController, animated: true, completion: nil)
    }
  }
  
}

// MARK: - UICollectionViewDataSource

extension FeedViewController: UICollectionViewDataSource {
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    // Count the number of images and generate the correct amount of cells.
    return feedArray.count
  }
  // indexPath.row still works even if multiple cells per row.
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    // Create a cell of type FeedCell.
    let cell: FeedCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! FeedCell
    // Get an item from the feedArray.
    let thisItem = feedArray[indexPath.row] as! FeedItem
    // Extract the image and caption from the item to populate/update the cell.
    cell.imageView.image = UIImage(data: thisItem.image) // convert the data back to a UIImage
    cell.captionLabel.text = thisItem.caption
    return cell
  }
}

// MARK: - UICollectionViewDelegate

extension FeedViewController: UICollectionViewDelegate {
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    let thisItem = feedArray[indexPath.row] as! FeedItem
    let filterVC = FilterViewController()
    filterVC.thisFeedItem = thisItem
    // ? because to make sure if not in navigation stack
    self.navigationController?.pushViewController(filterVC, animated: false)
  }
}

// MARK: - UIImagePickerControllerDelegate

extension FeedViewController: UIImagePickerControllerDelegate {
  // Get the image, convert it to binary data so it's persist-able, in order to create the FeedItem need to
  // get managedObjectContext back from AppDelegate, create an entity description, make the Feeditem and then
  // save it.
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
    // info is a dictionary that can pass back a cropped image for example, if user-editing was allowed.
    let image = info[UIImagePickerControllerOriginalImage] as! UIImage
    
    // Now convert the UIImage to data so it can actually be stored in file system.
    // Converts UIImage instance into JPEGRepresentation compressed at 1.0.
    let imageData = UIImageJPEGRepresentation(image, 1.0)
    
    // low quality (0.1) thumbnail
    let thumbNailData = UIImageJPEGRepresentation(image, 0.1)
    
    // Get managedobjectcontext from AppDelegate.
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    // managedObjectContext is an optional because it might not exist in AppDelegate.
    let entityDescription = NSEntityDescription.entityForName("FeedItem", inManagedObjectContext: managedObjectContext!)
    
    // also a chance that the entityDescription didn't get created properly, so has to be an optional.
    let feedItem = FeedItem(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext!)
    feedItem.image = imageData
    feedItem.caption = "test caption"
    feedItem.thumbNail = thumbNailData
    (UIApplication.sharedApplication().delegate as! AppDelegate).saveContext()
    
    // so photo appears first time it's added
    feedArray.append(feedItem)
    self.collectionView.reloadData()
    
    self.dismissViewControllerAnimated(true, completion: nil)
  }
}

// MARK: - UINavigationControllerDelegate
//
// UIImagePickerController is a subclass of UINavigationController.
// Empty implementation needed for ability to exit out of UIImagePickerController.

extension FeedViewController: UINavigationControllerDelegate { }
