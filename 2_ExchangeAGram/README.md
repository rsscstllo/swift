# README #

### About This Repository ###

* A picture-taking app that allowed the user to take pictures, choose from a page of filters that loaded example thumbnails via multi-threading (GCD), and save the pictures to Core Data –– followed along with, but did not implement, Facebook integration part of tutorial to share photos online with friends.  
###
* Two main view controllers with UICollectionViews holding photos.  
