//
//  DetailViewController.swift
//  FoodTracker
//
//  Created by Ross Castillo on 6/27/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
  
  @IBOutlet weak var textView: UITextView!
  
  var usdaItem: USDAItem?
  
  // NSCoder is needed to grab this VC from the storyboard - it allows Xcode to properly unpack the 
  // ViewController from the storyboard.
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "usdaItemDidComplete:", name: kUSDAItemCompleted, object: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    
    // Check to make sure that usdaitem is not nil before attempting to update the text view
    if usdaItem != nil {
      textView.attributedText = createAttributedString(usdaItem!)
    }
  }
  
  // Since NSNotificationCenter isn't managed by ARC. Manually remove ourselves to not send messages to a view
  // controller that no longer exists.
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(self)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func usdaItemDidComplete(notification: NSNotification) {
    println("usdaItemDidComplete in DetailViewController")
    usdaItem = notification.object as? USDAItem
    if self.isViewLoaded() && self.view.window != nil {
      textView.attributedText = createAttributedString(usdaItem!)
    }
  }
  
  @IBAction func eatItBarButtonItemPressed(sender: UIBarButtonItem) { }
  
  func createAttributedString (usdaItem: USDAItem) -> NSAttributedString {
    let itemAttributedString = NSMutableAttributedString()
    let centeredParagraphStyle = NSMutableParagraphStyle()
    centeredParagraphStyle.alignment = NSTextAlignment.Center
    centeredParagraphStyle.lineSpacing = 10.0
    
    // Dictionary of keys and values
    let titleAttributesDictionary = [NSForegroundColorAttributeName : UIColor.blackColor(), NSFontAttributeName : UIFont.boldSystemFontOfSize(22.0), NSParagraphStyleAttributeName : centeredParagraphStyle]
    
    let titleString = NSAttributedString(string: "\(usdaItem.name)\n", attributes: titleAttributesDictionary)
    itemAttributedString.appendAttributedString(titleString)
    
    let leftAllignedParagraphStyle = NSMutableParagraphStyle()
    leftAllignedParagraphStyle.alignment = NSTextAlignment.Left
    leftAllignedParagraphStyle.lineSpacing = 20.0
    // Set up 3 dictionaries
    let styleFirstWordAttributesDictionary = [NSForegroundColorAttributeName : UIColor.blackColor(), NSFontAttributeName : UIFont.boldSystemFontOfSize(18.0),
      NSParagraphStyleAttributeName : leftAllignedParagraphStyle]
    let style1AttributesDictionary = [NSForegroundColorAttributeName : UIColor.darkGrayColor(), NSFontAttributeName : UIFont.systemFontOfSize(18.0),
      NSParagraphStyleAttributeName : leftAllignedParagraphStyle]
    let style2AttributesDictionary = [NSForegroundColorAttributeName : UIColor.lightGrayColor(), NSFontAttributeName : UIFont.systemFontOfSize(18.0),
      NSParagraphStyleAttributeName : leftAllignedParagraphStyle]
    
    // Calcium
    let calciumTitleString = NSAttributedString(string: "Calcium ", attributes: styleFirstWordAttributesDictionary)
    let calciumBodyString = NSAttributedString(string: "\(usdaItem.calcium)% \n", attributes: style1AttributesDictionary)
    itemAttributedString.appendAttributedString(calciumTitleString)
    itemAttributedString.appendAttributedString(calciumBodyString)
    
    // Carbohydrate
    let carbohydrateTitleString = NSAttributedString(string: "Carbohydrate ", attributes: styleFirstWordAttributesDictionary)
    let carbohydrateBodyString = NSAttributedString(string: "\(usdaItem.carbohydrate)% \n", attributes: style2AttributesDictionary)
    itemAttributedString.appendAttributedString(carbohydrateTitleString)
    itemAttributedString.appendAttributedString(carbohydrateBodyString)
    
    // Cholesterol
    let cholesterolTitleString = NSAttributedString(string: "Cholesterol ", attributes: styleFirstWordAttributesDictionary)
    let cholesterolBodyString = NSAttributedString(string: "\(usdaItem.cholesterol)% \n", attributes: style1AttributesDictionary)
    itemAttributedString.appendAttributedString(cholesterolTitleString)
    itemAttributedString.appendAttributedString(cholesterolBodyString)
    
    // Energy
    let energyTitleString = NSAttributedString(string: "Energy ", attributes: styleFirstWordAttributesDictionary)
    let energyBodyString = NSAttributedString(string: "\(usdaItem.energy)% \n", attributes: style2AttributesDictionary)
    itemAttributedString.appendAttributedString(energyTitleString)
    itemAttributedString.appendAttributedString(energyBodyString)
    
    // Fat Total
    let fatTotalTitleString = NSAttributedString(string: "FatTotal ", attributes: styleFirstWordAttributesDictionary)
    let fatTotalBodyString = NSAttributedString(string: "\(usdaItem.fatTotal)% \n", attributes: style1AttributesDictionary)
    itemAttributedString.appendAttributedString(fatTotalTitleString)
    itemAttributedString.appendAttributedString(fatTotalBodyString)
    
    // Protein
    let proteinTitleString = NSAttributedString(string: "Protein ", attributes: styleFirstWordAttributesDictionary)
    let proteinBodyString = NSAttributedString(string: "\(usdaItem.protein)% \n", attributes: style2AttributesDictionary)
    itemAttributedString.appendAttributedString(proteinTitleString)
    itemAttributedString.appendAttributedString(proteinBodyString)
    
    // Sugar
    let sugarTitleString = NSAttributedString(string: "Sugar ", attributes: styleFirstWordAttributesDictionary)
    let sugarBodyString = NSAttributedString(string: "\(usdaItem.sugar)% \n", attributes: style1AttributesDictionary)
    itemAttributedString.appendAttributedString(sugarTitleString)
    itemAttributedString.appendAttributedString(sugarBodyString)
    
    // Vitamin C
    let vitaminCTitleString = NSAttributedString(string: "Vitamin C ", attributes: styleFirstWordAttributesDictionary)
    let vitaminCBodyString = NSAttributedString(string: "\(usdaItem.vitaminC)% \n", attributes: style2AttributesDictionary)
    itemAttributedString.appendAttributedString(vitaminCTitleString)
    itemAttributedString.appendAttributedString(vitaminCBodyString)
    
    return itemAttributedString
  }
}
