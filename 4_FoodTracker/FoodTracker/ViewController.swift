//
//  ViewController.swift
//  FoodTracker
//
//  Created by Ross Castillo on 6/27/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  var searchController: UISearchController!
  
  var suggestedSearchFoods: [String] = []
  var filteredSuggestedSearchFoods: [String] = []
  
  var scopeButtonTitles = ["Recommended", "Search Results", "Saved"]
  
  var jsonResponse: NSDictionary!
  var apiSearchForFoods: [(name: String, idValue: String)] = []
  
  let kAppId = "9c42395a"
  let kAppKey = "a1c80a6b70f8e732969b7ba58d2540a1"
  
  var dataController = DataController()
  
  var favoritedUSDAItems: [USDAItem] = []
  var filteredFavoritedUSDAItems:[USDAItem] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    tableView.dataSource = self
    tableView.delegate = self
    
    searchController = UISearchController(searchResultsController: nil)
    searchController.searchResultsUpdater = self
    searchController.dimsBackgroundDuringPresentation = false
    searchController.hidesNavigationBarDuringPresentation = false
    searchController.searchBar.frame = CGRectMake(searchController.searchBar.frame.origin.x, searchController.searchBar.frame.origin.y, searchController.searchBar.frame.size.width, 44.0)
    
    // Update the table view header with the search bar
    tableView.tableHeaderView = searchController.searchBar
    
    searchController.searchBar.scopeButtonTitles = scopeButtonTitles
    searchController.searchBar.delegate = self
    
    // Make sure the search results controller is presented inside the new/current view controller
    definesPresentationContext = true
    
    self.suggestedSearchFoods = ["apple", "bagel", "banana", "beer", "bread", "carrots", "cheddar cheese", "chicken breast", "chili with beans", "chocolate chip cookie", "coffee", "cola", "corn", "egg", "graham cracker", "granola bar", "green beans", "ground beef patty", "hot dog", "ice cream", "jelly doughnut", "ketchup", "milk", "mixed nuts", "mustard", "oatmeal", "orange juice", "peanut butter", "pizza", "pork chop", "potato", "potato chips", "pretzels", "raisins", "ranch salad dressing", "red wine", "rice", "salsa", "shrimp", "spaghetti", "spaghetti sauce", "tuna", "white wine", "yellow cake"]
    
    // Set up this view controller to start listening
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "usdaItemDidComplete:", name: kUSDAItemCompleted, object: nil)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "toDetailVCSegue" {
      if sender != nil {
        let detailVC = segue.destinationViewController as! DetailViewController
        detailVC.usdaItem = sender as? USDAItem
      }
    }
  }
  
  // MARK: - Nutritionix API
  
//  // Pass in a search string from search bar for a HTTP GET Request
//  func makeRequest (searchString : String) {
//    let url = NSURL(string: "https://api.nutritionix.com/v1_1/search/\(searchString)?results=0%3A20&cal_min=0&cal_max=50000&fields=item_name%2Cbrand_name%2Citem_id%2Cbrand_id&appId=9c42395a&appKey=a1c80a6b70f8e732969b7ba58d2540a1")
//    let task = NSURLSession.sharedSession().dataTaskWithURL(url!, completionHandler: { (data, response, error) -> Void in
//      var stringData = NSString(data: data, encoding: NSUTF8StringEncoding)
//      println(stringData)
//      println(response)
//    })
//    task.resume()
//  }
  
  // Pass in a search string from search bar for a HTTP POST Request
  func makeRequest (searchString : String) {
    var request = NSMutableURLRequest(URL: NSURL(string: "https://api.nutritionix.com/v1_1/search/")!)
    let session = NSURLSession.sharedSession()
    request.HTTPMethod = "POST"
    // Set up dictionary specifying the fields to be returned
    var params = [
      "appId"  : kAppId,
      "appKey" : kAppKey,
      "fields" : ["item_name", "brand_name", "keywords", "usda_fields"],
      "limit"  : "50",
      "query"  : searchString,
      "filters": ["exists":["usda_fields": true]]]
    var error: NSError?
    // Tell the request it is the correct type of JSON data
    request.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &error)
    // addValue helps format our HTTP headers so that the browser likes them
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    // Show that the app is loading data
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    
    // Create a variable passing in the request.
    // Inside completion handler, create a string data with the decoding that allows conversion from NSData
    // to NSString.
    // Create an NSError to put in JSON dictionary.
    var task = session.dataTaskWithRequest(request, completionHandler: { (data, response, err) -> Void in
      // print out the string representation, to compare with printing out the dictionary
      //var stringData = NSString(data: data, encoding: NSUTF8StringEncoding)
      //println(stringData)
      var conversionError: NSError?
      var jsonDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves, error: &conversionError) as? NSDictionary
      //println(jsonDictionary)
      
      // if statement that will make sure we can convert our data to json. In the case where we get some other data back, we will know it.
      // For example, maybe XML is returned, so this error happens since it cant be converted/parsed.
      if conversionError != nil {
        println(conversionError!.localizedDescription)
        let errorString = NSString(data: data, encoding: NSUTF8StringEncoding)
        println("Error in Parsing \(errorString)")
      }
      else {
        if jsonDictionary != nil {
          // Store json dictionary in jsonResponse property
          self.jsonResponse = jsonDictionary!
          // array of tuples for all different usda items
          self.apiSearchForFoods = DataController.jsonAsUSDAIdAndNameSearchResults(jsonDictionary!)
          
          // Show the "spinner" loading icon in the status bar.
          UIApplication.sharedApplication().networkActivityIndicatorVisible = false
          
          // Upload UI on main thread.
          dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
          })
        }
        else {
          let errorString = NSString(data: data, encoding: NSUTF8StringEncoding)
          println("Error Could not Parse JSON \(errorString)")
        }                
      }
    })
    task.resume()
  }
  
  // MARK - Setup CoreData
  
  // Retrieve usda items from core data
  func requestFavoritedUSDAItems() {
    // Import CoreData to use NSFetchRequest
    let fetchRequest = NSFetchRequest(entityName: "USDAItem")
    let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
    let managedObjectContext = appDelegate.managedObjectContext
    self.favoritedUSDAItems = managedObjectContext?.executeFetchRequest(fetchRequest, error: nil) as! [USDAItem]
  }
  
  // Mark: - NSNotificationCenter
  
  func usdaItemDidComplete(notification : NSNotification) {
    println("usdaItemDidComplete in ViewController")
    requestFavoritedUSDAItems()
    let selectedScopeButtonIndex = self.searchController.searchBar.selectedScopeButtonIndex
    if selectedScopeButtonIndex == 2 {
      self.tableView.reloadData()
    }
  }
  
}

// MARK: - UITableViewDataSource

extension ViewController: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let selectedScopeButtonIndex = self.searchController.searchBar.selectedScopeButtonIndex
    
    if selectedScopeButtonIndex == 0 {
      if self.searchController.active {
        return self.filteredSuggestedSearchFoods.count
      }
      else {
        return self.suggestedSearchFoods.count
      }
    }
    else if selectedScopeButtonIndex == 1 {
      return self.apiSearchForFoods.count
    }
    else {
      if self.searchController.active && self.searchController.searchBar.text != "" {
        return self.filteredFavoritedUSDAItems.count
      }
      else {
        return self.favoritedUSDAItems.count
      }
    }
  }
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! UITableViewCell
    var foodName : String
    // Get current index that's selcted in the scope buttons
    let selectedScopeButtonIndex = searchController.searchBar.selectedScopeButtonIndex
    
    if selectedScopeButtonIndex == 0 {
      if searchController.active {
        foodName = filteredSuggestedSearchFoods[indexPath.row]
      }
      else {
        foodName = suggestedSearchFoods[indexPath.row]
      }
    }
    else if selectedScopeButtonIndex == 1 {
      foodName = apiSearchForFoods[indexPath.row].name
    }
    else {
      if self.searchController.active && self.searchController.searchBar.text != "" {
        foodName = self.filteredFavoritedUSDAItems[indexPath.row].name
      }
      else {
        foodName = self.favoritedUSDAItems[indexPath.row].name
      }
    }
    
    cell.textLabel?.text = foodName
    cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
    return cell
  }
}

// MARK: - UITableViewDelegate

extension ViewController: UITableViewDelegate {
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    // Get access to scope button index to code different functionality for each scope
    let selectedScopeButtonIndex = searchController.searchBar.selectedScopeButtonIndex
    
    if selectedScopeButtonIndex == 0 {
      var searchFoodName: String
      
      if self.searchController.active {
        searchFoodName = filteredSuggestedSearchFoods[indexPath.row]
      }
      else {
        searchFoodName = suggestedSearchFoods[indexPath.row]
      }
      searchController.searchBar.selectedScopeButtonIndex = 1
      makeRequest(searchFoodName)
    }
    else if selectedScopeButtonIndex == 1 {
      self.performSegueWithIdentifier("toDetailVCSegue", sender: nil)
      // Need the idValue to call the function
      let idValue = apiSearchForFoods[indexPath.row].idValue
      self.dataController.saveUSDAItemForId(idValue, json: self.jsonResponse)
    }
    else if selectedScopeButtonIndex == 2 {
      if self.searchController.active && self.searchController.searchBar.text != ""{
        let usdaItem = filteredFavoritedUSDAItems[indexPath.row]
        self.performSegueWithIdentifier("toDetailVCSegue", sender: usdaItem)
      }
      else {
        let usdaItem = favoritedUSDAItems[indexPath.row]
        self.performSegueWithIdentifier("toDetailVCSegue", sender: usdaItem)
      }
    }
  }
  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) { }
}

// MARK: - UISearchBarDelegate

extension ViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(searchBar: UISearchBar) {
    searchController.searchBar.selectedScopeButtonIndex = 1
    makeRequest(searchBar.text)
  }
  func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    if selectedScope == 2 {
      requestFavoritedUSDAItems()
    }
    self.tableView.reloadData()
  }
}

// MARK: - UISearchControllerDelegate

extension ViewController: UISearchControllerDelegate { }

// MARK: - UISearchResultsUpdating

extension ViewController: UISearchResultsUpdating {
  func updateSearchResultsForSearchController(searchController: UISearchController) {
    let searchString = self.searchController.searchBar.text
    let selectedScopeButtonIndex = self.searchController.searchBar.selectedScopeButtonIndex
    if searchString == "" {
      filteredSuggestedSearchFoods = suggestedSearchFoods
    } else {
      filterContentForSearch(searchString, scope: selectedScopeButtonIndex)
    }
    tableView.reloadData()
  }
  func filterContentForSearch (searchText: String, scope: Int) {
    if scope == 0 {
      filteredSuggestedSearchFoods = suggestedSearchFoods.filter({ (food: String) -> Bool in
        let foodMatch = food.rangeOfString(searchText)
        return foodMatch != nil
      })
    } else if scope == 2 {
      filteredFavoritedUSDAItems = favoritedUSDAItems.filter({ (item: USDAItem) -> Bool in
        let stringMatch = item.name.rangeOfString(searchText)
        return stringMatch != nil
      })
    }
  }
}
