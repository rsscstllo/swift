//
//  DataController.swift
//  FoodTracker
//
//  Created by Ross Castillo on 6/27/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// Doing this above the class gives other classes access
let kUSDAItemCompleted = "USDAItemInstanceComplete"

// Responsible for the dictionary returned from indexing into the json response.
class DataController {
  // Pass in a dictionary and return an array of tuples (key, value).
  class func jsonAsUSDAIdAndNameSearchResults(json: NSDictionary) -> [(name: String, idValue: String)] {
    
    var usdaItemsSearchResults:[(name: String, idValue: String)] = []
    var searchResult: (name: String, idValue: String)
    
    // Had to look at the returned json output in the console to know how to properly index into the array
    if json["hits"] != nil {
      // "hits" is an array of dictionaries, but super complicated, so AnyObject is used.
      let results: [AnyObject] = json["hits"]! as! [AnyObject]
      
      // Iterate over the results array
      for itemDictionary in results {
        
        // Make sure the _id key exists
        if itemDictionary["_id"] != nil {
          
          // Make sure the fields key exists, which holds a dictionary
          if itemDictionary["fields"] != nil {
            // Since the fields key does exist, assign it to a dictionary variable, that contains dictionaries
            let fieldsDictionary = itemDictionary["fields"] as! NSDictionary
            
            if fieldsDictionary["item_name"] != nil {
              // Have to unwrap since there's a chance fieldsDictionary is nil
              let idValue: String = itemDictionary["_id"]! as! String
              let name: String = fieldsDictionary["item_name"]! as! String
              searchResult = (name: name, idValue: idValue)
              // Add the tuple to the array
              usdaItemsSearchResults += [searchResult]
            }
          }
        }
      }
    }
    // return the array of tuples
    return usdaItemsSearchResults
  }
  
  func saveUSDAItemForId(idValue: String, json: NSDictionary) {
    if json["hits"] != nil {
      let results: [AnyObject] = json["hits"]! as! [AnyObject]
      for itemDictionary in results {
        
        // check that there is an id and that the item coming back is a string object
        if itemDictionary["_id"] != nil && itemDictionary["_id"] as! String == idValue {
          // confirm that this dictionary hasnt already ben saved to core data
          let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
          var requestForUSDAItem = NSFetchRequest(entityName: "USDAItem")
          
          let itemDictionaryId = itemDictionary["_id"]! as! String
          let predicate = NSPredicate(format: "idValue == %@", itemDictionaryId)
          requestForUSDAItem.predicate = predicate
          
          var error: NSError?
          var items = managedObjectContext?.executeFetchRequest(requestForUSDAItem, error: &error)
          
          if items?.count != 0 {
            // The item is already saved, just return/end the function, since we don't want to save it again.
            return
          }
          else {
            println("Save this to CoreData")
            // Create an instance of USDAItem to assign attributes to it.
            let entityDescription = NSEntityDescription.entityForName("USDAItem", inManagedObjectContext: managedObjectContext!)
            let usdaItem = USDAItem(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext!)
            usdaItem.idValue = itemDictionary["_id"]! as! String
            
            usdaItem.dateAdded = NSDate()
            if itemDictionary["fields"] != nil {
              let fieldsDictionary = itemDictionary["fields"]! as! NSDictionary
              if fieldsDictionary["item_name"] != nil {
                usdaItem.name = fieldsDictionary["item_name"]! as! String
              }
              
              if fieldsDictionary["usda_fields"] != nil {
                let usdaFieldsDictionary = fieldsDictionary["usda_fields"]! as! NSDictionary
                if usdaFieldsDictionary["CA"] != nil {
                  let calciumDictionary = usdaFieldsDictionary["CA"]! as! NSDictionary
                  let calciumValue: AnyObject = calciumDictionary["value"]!
                  // Potential chance that value could be a number or a string, or other type, so interpolation
                  usdaItem.calcium = "\(calciumValue)"
                }
                else {  // If it's nil
                  // Just keep as string for now since json is all in strings, do conversion later elsewhere
                  usdaItem.calcium = "0"
                }
                
                // Carbohydrate Grouping
                if usdaFieldsDictionary["CHOCDF"] != nil {
                  let carbohydrateDictionary = usdaFieldsDictionary["CHOCDF"]! as! NSDictionary
                  if carbohydrateDictionary["value"] != nil {
                    let carbohydrateValue: AnyObject = carbohydrateDictionary["value"]!
                    usdaItem.carbohydrate = "\(carbohydrateValue)"
                  }
                }
                else {
                  usdaItem.carbohydrate = "0"
                }
                
                // Fat Grouping
                if usdaFieldsDictionary["FAT"] != nil {
                  let fatTotalDictionary = usdaFieldsDictionary["FAT"]! as! NSDictionary
                  if fatTotalDictionary["value"] != nil {
                    let fatTotalValue:AnyObject = fatTotalDictionary["value"]!
                    usdaItem.fatTotal = "\(fatTotalValue)"
                  }
                }
                else {
                  usdaItem.fatTotal = "0"
                }
                
                // Cholesterol Grouping
                if usdaFieldsDictionary["CHOLE"] != nil {
                  let cholesterolDictionary = usdaFieldsDictionary["CHOLE"]! as! NSDictionary
                  if cholesterolDictionary["value"] != nil {
                    let cholesterolValue: AnyObject = cholesterolDictionary["value"]!
                    usdaItem.cholesterol = "\(cholesterolValue)"
                  }
                }
                else {
                  usdaItem.cholesterol = "0"
                }
                
                // Protein Grouping
                if usdaFieldsDictionary["PROCNT"] != nil {
                  let proteinDictionary = usdaFieldsDictionary["PROCNT"]! as! NSDictionary
                  if proteinDictionary["value"] != nil {
                    let proteinValue: AnyObject = proteinDictionary["value"]!
                    usdaItem.protein = "\(proteinValue)"
                  }
                }
                else {
                  usdaItem.protein = "0"
                }
                
                // Sugar Total Grouping
                if usdaFieldsDictionary["SUGAR"] != nil {
                  let sugarDictionary = usdaFieldsDictionary["SUGAR"]! as! NSDictionary
                  if sugarDictionary["value"] != nil {
                    let sugarValue:AnyObject = sugarDictionary["value"]!
                    usdaItem.sugar = "\(sugarValue)"
                  }
                }
                else {
                  usdaItem.sugar = "0"
                }
                
                // Vitamin C Grouping
                if usdaFieldsDictionary["VITC"] != nil {
                  let vitaminCDictionary = usdaFieldsDictionary["VITC"]! as! NSDictionary
                  if vitaminCDictionary["value"] != nil {
                    let vitaminCValue: AnyObject = vitaminCDictionary["value"]!
                    usdaItem.vitaminC = "\(vitaminCValue)"
                  }
                }
                else {
                  usdaItem.vitaminC = "0"
                }
                
                // Energy Grouping
                if usdaFieldsDictionary["ENERC_KCAL"] != nil {
                  let energyDictionary = usdaFieldsDictionary["ENERC_KCAL"]! as! NSDictionary
                  if energyDictionary["value"] != nil {
                    let energyValue: AnyObject = energyDictionary["value"]!
                    usdaItem.energy = "\(energyValue)"
                  }
                }
                else {
                  usdaItem.energy = "0"
                }
                
                // Save USDAItem instance
                (UIApplication.sharedApplication().delegate as! AppDelegate).saveContext()
                
                NSNotificationCenter.defaultCenter().postNotificationName(kUSDAItemCompleted, object: usdaItem)
              }
            }
          }
        }
      }
    }
  }
}
