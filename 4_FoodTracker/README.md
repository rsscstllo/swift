# README #

### About This Repository ###

* An app to track the nutritional information of food you've eaten. Nutritritional information pulled from the nutritionix API (http://www.nutritionix.com) –– followed along with, but did not implement, HealthKit and NSNotificationCenter parts of tutorial.
###
* Two view controllers: one with a UITableView and UISearchController that displayed food items, and the other with a UITextView that displayed the information for a selected food item.  
