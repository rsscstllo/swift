//
//  Factory.swift
//  SlotMachine
//
//  Created by Ross Castillo on 5/10/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

// NOTES:
// A class func is called on a factory class, while an instance function is called on an instance
// of a class. NOTE: can't access class properties inside of class functions.
//
// Example of double array:
// slots = [ [slot1, slot2, slot3], [slot4, slot5, slot6], [slot7, slot8, slot9] ]
// mySlotArray = slots[0]  // Index into an array inside of the main array.
// mySlot = mySlotArray[1] // Index into that arrays index.

import Foundation
import UIKit

class Factory {
  // Class function to create all 9 slots.
  class func createSlots() -> [[Slot]] {
    // Create an empty array that will hold arrays of slot instances.
    var slots: [[Slot]] = []
    
    // Create 3 empty arrays that will hold Slot instances.
    for var i = 0; i < 3; ++i {
      var slotArray: [Slot] = []
      
      // Create 3 slot instances via createSlot() and add them to the slotArray.
      for var j = 0; j < 3; ++j {
        let slot = createSlot(slotArray)
        slotArray.append(slot)
      }
      // Add the slot array (column) to the main slots array.
      slots.append(slotArray)
    }
    return slots
  }
  
  // Class function responsible for creating slot instances. Pass in an array of Slot instances and
  // return a slot. This is to prevent a duplicate card in an inner array (column) of slots.
  class func createSlot(currentCards: [Slot]) -> Slot {
    // Empty array that holds integers.
    var currentCardValues:[Int] = []
    
    // Fast-enumeration for loop to iterate through all of the slot instances in the currentCards
    // array and get the value for each card to add to the currentCardValues array.
    for slot in currentCards {
      currentCardValues.append(slot.value)
    }
    
    // Create a random number from 0-12.
    var randomNumber = Int(arc4random_uniform(UInt32(13)))
    
    // Check if the currentCardValues array contains that random number + 1 since the values are 
    // from 1-13 not 0-12.
    while currentCardValues.contains(randomNumber + 1) {
      // Update the randomNumber until it is a number that's not currently contained in the array.
      // This ensures duplicates will only occur across a row, and not down a column.
      randomNumber = Int(arc4random_uniform(UInt32(13)))
    }
    
    // Create a Slot instance to be initialized.
    var slot: Slot
    
    // switch statement to determine the corresponding value and image of the card based on the 
    // random number.
    switch randomNumber {
    case 0:
      slot = Slot(value: 1, image: UIImage(named: "Ace"), isRed: true)
    case 1:
      slot = Slot(value: 2, image: UIImage(named: "Two"), isRed: true)
    case 2:
      slot = Slot(value: 3, image: UIImage(named: "Three"), isRed: true)
    case 3:
      slot = Slot(value: 4, image: UIImage(named: "Four"), isRed: true)
    case 4:
      slot = Slot(value: 5, image: UIImage(named: "Five"), isRed: false)
    case 5:
      slot = Slot(value: 6, image: UIImage(named: "Six"), isRed: false)
    case 6:
      slot = Slot(value: 7, image: UIImage(named: "Seven"), isRed: true)
    case 7:
      slot = Slot(value: 8, image: UIImage(named: "Eight"), isRed: false)
    case 8:
      slot = Slot(value: 9, image: UIImage(named: "Nine"), isRed: false)
    case 9:
      slot = Slot(value: 10, image: UIImage(named: "Ten"), isRed: true)
    case 10:
      slot = Slot(value: 11, image: UIImage(named: "Jack"), isRed: false)
    case 11:
      slot = Slot(value: 12, image: UIImage(named: "Queen"), isRed: false)
    case 12:
      slot = Slot(value: 13, image: UIImage(named: "King"), isRed: true)
    default:
      slot = Slot(value: 0, image: UIImage(named: "Ace"), isRed: true)
    }
    return slot
  }
}
