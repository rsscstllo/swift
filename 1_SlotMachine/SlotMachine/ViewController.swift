//
//  ViewController.swift
//  SlotMachine
//
//  Created by Ross Castillo on 5/8/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import UIKit
// Other .swift files are automatically imported, so no need to import them here.

class ViewController: UIViewController {
  
  // The first view will hold the title label.
  // The second view will contain 9 UIImageView's that will randomly generate card slots.
  // The thrid view will have three labels: Credits, Bet, Payout
  // The fourth view will have 4 buttons: Reset, Bet One, Bet Max, Spin.
  // NOTE: Variables not actually created in memory yet, just made a name for each type of variable.
  var firstView: UIView!
  var secondView: UIView!
  var thirdView: UIView!
  var fourthView: UIView!
  
  // Information Lables
  var titleLabel: UILabel!
  var creditsLabel: UILabel!
  var betLabel: UILabel!
  var payoutLabel: UILabel!
  var creditsTitleLabel: UILabel!
  var betTitleLabel: UILabel!
  var payoutTitleLabel: UILabel!
  
  // Buttons in fourth view
  var resetButton: UIButton!
  var betOneButton: UIButton!
  var betMaxButton: UIButton!
  var spinButton: UIButton!
  
  // Property to hold all of the slots set equal to an empty array.
  var slots: [[Slot]] = []
  
  // IBOutlets/Variables to keep track of user's stats.
  var credits = 0
  var currentBet = 0
  var winnings = 0
  
  // Constants for setting up UI. kMarginForView gives a 10 point margin on each side of view and
  // kSixth helps set up each view's height proportionally.
  // CGFloat typedef's (transforms) to a float or a double, depending on your system
  // 64-bit = double, 32-bit = float. Used since view's frame parameters accept CGFloats.
  let kMarginForView:CGFloat  = 10.0
  let kMarginForSlot:CGFloat  = 2.0
  let kSixth:CGFloat          = 1.0 / 6.0
  let kThird:CGFloat          = 1.0 / 3.0
  let kHalf:CGFloat           = 1.0 / 2.0
  let kEighth:CGFloat         = 1.0 / 8.0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    // Call helper functions.
    self.createViews()
    self.setupFirstView()
    self.setupThirdView()
    self.setupFourthView()
    self.hardReset() // Sets up second view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - IBActions
  
  func resetButtonPressed(button: UIButton) {
    // println("Reset Button Pressed")
    hardReset()
  }
  
  // Bet a single credit at a time.
  func betOneButtonPressed(button: UIButton) {
    // println(button)
    // println("Bet One Button Pressed")
    
    // If the user has no more credit.
    if self.credits <= 0 {
      showAlertWithText("No More Credits", message: "Reset Game")
      // If the user does have credit left.
    } else {
      // Allowing the user to only bet 5 credits at a time.
      if self.currentBet < 5 {
        self.currentBet += 1
        self.credits -= 1
        updateMainView()
      } else {
        showAlertWithText(message: "You can only bet 5 credits at a time!")
      }
    }
  }
  
  // Bet up to the nearest five credits.
  func betMaxButtonPressed(button: UIButton) {
    // println("Bet Max Button Pressed")
    
    if self.credits <= 0 {
      showAlertWithText("No More Credits", message: "Reset Game")
    } else {
      // If the user has already bet up to 5.
      if self.currentBet == 5 {
        showAlertWithText(message: "You can only bet 5 credits at a time!")
        
        // If the current bet is not yet 5.
      } else {
        // If the user's credits are greater than or equal to the 5 minus the current bet.
        if self.credits >= (5 - self.currentBet) {
          
          // While the current bet is not 5, increment it until it is.
          while self.currentBet != 5 {
            self.currentBet += 1
            self.credits -= 1
          }
          // Update the labels with the new information.
          updateMainView()
          
          // If the user's credits are less than 5 minus the current bet.
        } else {
          showAlertWithText("Not Enough Credits", message: "Bet Less")
        }
      }
    }
  }
  
  // Create new slots and remove the old ones every time the spin button is pressed.
  func spinButtonPressed(button: UIButton) {
    // println("Spin Button Pressed")
    removeSlotImageViews()
    self.slots = Factory.createSlots()
    setupSecondView()
    
    let winningMultiplier = Winnings.computeWinnings(slots)
    self.winnings = winningMultiplier * self.currentBet
    self.credits += self.winnings
    self.currentBet = 0
    updateMainView()
  }
  
  // MARK: - Helper Functions
  
  // Helper function to set up the main 4 UIView's, specifying where they should be drawn on the VC.
  // (x, y) refers to the position of the current view in relation to its immediate parent/super
  // view, starting from (0, 0) at the top left of the parent/super view and increasing to the right
  // and down. Conversely, bounds are relative to the current view. NOTE: the self keyword does not
  // have to be used for accessing instance variables.
  func createViews()  {
    // Initialize the views.
    self.firstView  = UIView(frame: CGRect(x: self.view.bounds.origin.x + kMarginForView,
                                           y: self.view.bounds.origin.y,
                             width: self.view.bounds.width - (kMarginForView * 2),
                            height: self.view.bounds.height * kSixth))

    self.secondView = UIView(frame: CGRect(x: self.view.bounds.origin.x + kMarginForView,
                                           y: firstView.frame.height,
                             width: self.view.bounds.width - (kMarginForView * 2),
                            height: self.view.bounds.height * (3 * kSixth)))

    self.thirdView  = UIView(frame: CGRect(x: self.view.bounds.origin.x + kMarginForView,
                                           y: firstView.frame.height +
                                              secondView.frame.height,
                             width: self.view.bounds.width - (kMarginForView * 2),
                            height: self.view.bounds.height * kSixth))

    self.fourthView = UIView(frame: CGRect(x: self.view.bounds.origin.x + kMarginForView,
                                           y: firstView.frame.height +
                                              secondView.frame.height +
                                              thirdView.frame.height,
                             width: self.view.bounds.width - (kMarginForView * 2),
                            height: self.view.bounds.height * kSixth))
    
    // Set background colors of the views.
    self.firstView.backgroundColor  = UIColor.lightGrayColor()
    self.secondView.backgroundColor = UIColor.blackColor()
    self.thirdView.backgroundColor  = UIColor.lightGrayColor()
    self.fourthView.backgroundColor = UIColor.blackColor()
    
    // Finally, add the the views.
    self.view.addSubview(self.firstView)
    self.view.addSubview(self.secondView)
    self.view.addSubview(self.thirdView)
    self.view.addSubview(self.fourthView)
  }
  
  // Helper function to set up the title label in the first view.
  func setupFirstView() {
    self.titleLabel           = UILabel()
    self.titleLabel.text      = "Slot Machine"
    self.titleLabel.textColor = UIColor.blackColor()
    self.titleLabel.font      = UIFont(name: "MarkerFelt-Wide", size: 40)
    self.titleLabel.sizeToFit()
    self.titleLabel.center = self.firstView.center
    self.firstView.addSubview(self.titleLabel)
  }
  
  // Helper function to setup the 9 UIImageView's in the second view.
  func setupSecondView() {
    for var i = 0; i < 3; ++i {
      for var j = 0; j < 3; ++j {
        
        // Create a variable named Slot, not yet given a value, to represent each individual slot.
        var slot: Slot
        // Create a UIImageView for displaying the slot.
        let slotImageView = UIImageView()
        
        // If there are actually slots in the slot array.
        if slots.count != 0 {
          // Constant to represent the column of slots.
          let slotContainer = slots[i]
          // Get a slot instance from indexing into the column array.
          slot = slotContainer[j]
          // Display the slot's image.
          slotImageView.image = slot.image
        // If the slot array is empty, display default image.
        } else {
          slotImageView.image = UIImage(named: "Ace")
          // slotImageView.contentMode = UIViewContentMode.ScaleAspectFit
          // slotImageView.backgroundColor = UIColor.blackColor()
        }
        
        // Position the UIImageView appropriately.
        slotImageView.frame = CGRect(
          x: 1.0 +
             (secondView.bounds.origin.x +
             (secondView.bounds.size.width * CGFloat(i) * kThird)),
          y: 1.0 +
             (secondView.bounds.origin.y +
             (secondView.bounds.size.height * CGFloat(j) * kThird)),
          width: secondView.bounds.width * kThird - kMarginForSlot,
          height: secondView.bounds.height * kThird - kMarginForSlot)
        
        // Finally, add the UIImageView as a subview to the second view.
        secondView.addSubview(slotImageView)
      }
    }
  }
  
  // Helper function to remove UIViews upon generating new UIImageViews in order to prevent from
  // using too much memory keeping old views that arent needed anymore.
  func removeSlotImageViews () {
    // Additional check to make sure the second view is not empty.
    if self.secondView != nil {
      // Similar to above check but this one is required. Create an optional UIView to see if the
      // secondView actually does exist. Has to be an optional to be able to iterate through its 
      // subviews.
      let view: UIView? = self.secondView
      // Check if there are subview's in the view. Create an optional that is an array of views by
      // unwrapping the optional to access the subviews property.
      let subViews: Array? = view!.subviews
      // Fast enumeration to iterate through each subview.
      for view in subViews! {
        // Remove all subviews, or in this cases UIImageViews, added to the second view.
        view.removeFromSuperview()
      }
    }
  }
  
  // Helper function to set up the 6 labels in the third view.
  func setupThirdView() {
    // Initialize the instances of UILabel's created above.
    self.creditsLabel       = UILabel()
    self.betLabel           = UILabel()
    self.payoutLabel        = UILabel()
    self.creditsTitleLabel  = UILabel()
    self.betTitleLabel      = UILabel()
    self.payoutTitleLabel   = UILabel()
    
    // Set a default text to make a label at least this big.
    self.creditsLabel.text      = "000000"
    self.betLabel.text          = "0000"
    self.payoutLabel.text       = "000000"
    self.creditsTitleLabel.text = "Credits"
    self.betTitleLabel.text     = "Bet"
    self.payoutTitleLabel.text  = "Payout"
    
    // Set the text color for each label.
    self.creditsLabel.textColor       = UIColor.greenColor()
    self.betLabel.textColor           = UIColor.yellowColor()
    self.payoutLabel.textColor        = UIColor.greenColor()
    self.creditsTitleLabel.textColor  = UIColor.blackColor()
    self.betTitleLabel.textColor      = UIColor.blackColor()
    self.payoutTitleLabel.textColor   = UIColor.blackColor()
    
    // Set a background color for the main labels for contrast.
    self.creditsLabel.backgroundColor = UIColor.darkGrayColor()
    self.betLabel.backgroundColor     = UIColor.darkGrayColor()
    self.payoutLabel.backgroundColor  = UIColor.darkGrayColor()
    
    // Set the font for each label.
    self.creditsLabel.font      = UIFont(name: "Menlo-Bold", size: 16)
    self.betLabel.font          = UIFont(name: "Menlo-Bold", size: 16)
    self.payoutLabel.font       = UIFont(name: "Menlo-Bold", size: 16)
    self.creditsTitleLabel.font = UIFont(name: "AmericanTypewriter", size: 14)
    self.betTitleLabel.font     = UIFont(name: "AmericanTypewriter", size: 14)
    self.payoutTitleLabel.font  = UIFont(name: "AmericanTypewriter", size: 14)
    
    // Call the sizeToFit() function AFTER setting the font so it accomodates the text.
    self.creditsLabel.sizeToFit()
    self.betLabel.sizeToFit()
    self.payoutLabel.sizeToFit()
    self.creditsTitleLabel.sizeToFit()
    self.betTitleLabel.sizeToFit()
    self.payoutTitleLabel.sizeToFit()
    
    // Align the text to be centered when it's not all the way full.
    self.creditsLabel.textAlignment = NSTextAlignment.Center
    self.betLabel.textAlignment = NSTextAlignment.Center
    self.payoutLabel.textAlignment = NSTextAlignment.Center
    
    // Appropriately position each label in the view.
    self.creditsLabel.center      = CGPoint(x: thirdView.frame.width * kSixth,
                                            y: thirdView.frame.height * kThird)
    
    self.betLabel.center          = CGPoint(x: thirdView.frame.width * kSixth * 3,
                                            y: thirdView.frame.height * kThird)
    
    self.payoutLabel.center       = CGPoint(x: thirdView.frame.width * kSixth * 5,
                                            y: thirdView.frame.height * kThird)
    
    self.creditsTitleLabel.center = CGPoint(x: thirdView.frame.width * kSixth,
                                            y: thirdView.frame.height * kThird * 2)
    
    self.betTitleLabel.center     = CGPoint(x: thirdView.frame.width * kSixth * 3,
                                            y: thirdView.frame.height * kThird * 2)
    
    self.payoutTitleLabel.center  = CGPoint(x: thirdView.frame.width * 5 * kSixth,
                                            y: thirdView.frame.height * 2 * kThird)
    
    // Finally, add each label as a subview to the view.
    thirdView.addSubview(self.creditsLabel)
    thirdView.addSubview(self.betLabel)
    thirdView.addSubview(self.payoutLabel)
    thirdView.addSubview(self.creditsTitleLabel)
    thirdView.addSubview(self.betTitleLabel)
    thirdView.addSubview(self.payoutTitleLabel)
  }
  
  // Helper function for setting up the 3 buttons in the fourth view.
  func setupFourthView() {
    // Initialize the instances of UIButton's created above.
    self.resetButton  = UIButton()
    self.betOneButton = UIButton()
    self.betMaxButton = UIButton()
    self.spinButton   = UIButton()
    
    // Set a default text to make each button at least this big.
    self.resetButton.setTitle(" Reset ", forState: UIControlState.Normal)
    self.betOneButton.setTitle(" Bet One ", forState: UIControlState.Normal)
    self.betMaxButton.setTitle(" Bet Max ", forState: UIControlState.Normal)
    self.spinButton.setTitle(" Spin ", forState: UIControlState.Normal)
    
    // Set the text color for each button.
    self.resetButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    self.betOneButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    self.betMaxButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    self.spinButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
    
    // ? to make sure the title label attribute exists before setting the font.
    self.resetButton.titleLabel?.font  = UIFont(name: "Superclarendon-Bold", size: 12)
    self.betOneButton.titleLabel?.font = UIFont(name: "Superclarendon-Bold", size: 12)
    self.betMaxButton.titleLabel?.font = UIFont(name: "Superclarendon-Bold", size: 12)
    self.spinButton.titleLabel?.font = UIFont(name: "Superclarendon-Bold", size: 12)
    
    // Set the background color of each button.
    self.resetButton.backgroundColor  = UIColor.darkGrayColor()
    self.betOneButton.backgroundColor = UIColor.darkGrayColor()
    self.betMaxButton.backgroundColor = UIColor.darkGrayColor()
    self.spinButton.backgroundColor   = UIColor.darkGrayColor()
    
    // Round the corners of each button.
    self.resetButton.layer.cornerRadius  = 2.9
    self.betOneButton.layer.cornerRadius = 2.9
    self.betMaxButton.layer.cornerRadius = 2.9
    self.spinButton.layer.cornerRadius   = 2.9
    
    // Set the border width of each button.
    self.resetButton.layer.borderWidth  = 1.4
    self.betOneButton.layer.borderWidth = 1.4
    self.betMaxButton.layer.borderWidth = 1.4
    self.spinButton.layer.borderWidth   = 1.4
    
    // Set the border color of each button.
    self.resetButton.layer.borderColor  = UIColor.redColor().CGColor
    self.betOneButton.layer.borderColor = UIColor.whiteColor().CGColor
    self.betMaxButton.layer.borderColor = UIColor.whiteColor().CGColor
    self.spinButton.layer.borderColor   = UIColor.whiteColor().CGColor
    
    // Call the sizeToFit() function on each button to accomodate the default text.
    self.resetButton.sizeToFit()
    self.betOneButton.sizeToFit()
    self.betMaxButton.sizeToFit()
    self.spinButton.sizeToFit()
    
    // Appropriately position each label in the view.
    self.resetButton.center  = CGPoint(x: fourthView.frame.width * kEighth,
                                       y: fourthView.frame.height * kHalf)
    
    self.betOneButton.center = CGPoint(x: fourthView.frame.width * 3 * kEighth,
                                       y: fourthView.frame.height * kHalf)
    
    self.betMaxButton.center = CGPoint(x: fourthView.frame.width * 5 * kEighth,
                                       y: fourthView.frame.height * kHalf)
    
    self.spinButton.center   = CGPoint(x: fourthView.frame.width * 7 * kEighth,
                                       y: fourthView.frame.height * kHalf)
    
    
    // Add a target to self (this view controller) so when the user presses the button, the instance
    // of this view controller is notified. Need to add a colon after function name in quotes. The
    // colon specifies that a parameter will be passed in to the function, or action. Only one colon
    // is needed, even if there is more than one parameter. TouchUpInside for the control event for now
    // since it's the most common/default UIControlEvent.
    self.resetButton.addTarget(self, action: "resetButtonPressed:",
                           forControlEvents: UIControlEvents.TouchUpInside)
    
    self.betOneButton.addTarget(self, action: "betOneButtonPressed:",
                            forControlEvents: UIControlEvents.TouchUpInside)
    
    self.betMaxButton.addTarget(self, action: "betMaxButtonPressed:",
                            forControlEvents: UIControlEvents.TouchUpInside)
    
    self.spinButton.addTarget(self, action: "spinButtonPressed:",
                          forControlEvents: UIControlEvents.TouchUpInside)
    
    // Finally, add each button as a subview to the view.
    fourthView.addSubview(self.resetButton)
    fourthView.addSubview(self.betOneButton)
    fourthView.addSubview(self.betMaxButton)
    fourthView.addSubview(self.spinButton)
  }
  
  // Helper function to reset the game.
  func hardReset() {
    // Remove any slot image views if they exist.
    removeSlotImageViews()
    
    // Reset the slots array. keepCapacity should be true because the same amount of slots are going
    // to be added using the same amount of memory.
    self.slots.removeAll(keepCapacity: true)
    
    // Set up the second view.
    setupSecondView()
    
    // Reset stats.
    self.credits = 50
    self.winnings = 0
    self.currentBet = 0
    
    // Update labels every time the game is reset.
    updateMainView()
  }
  
  // Update the labels that display the game information via string interpolation.
  func updateMainView() {
    self.creditsLabel.text = "\(credits)"
    self.betLabel.text = "\(currentBet)"
    self.payoutLabel.text = "\(winnings)"
  }
  
  // Show an alert on the screen. Reqiured to pass in the message parameter, but the header is 
  // optional (defaults to "Warning" if not specified) and can be overridden.
  func showAlertWithText(header: String = "Warning", message: String) {
    // Initialize with the parameters passed in.
    let alert = UIAlertController(title: header,
                                message: message,
                         preferredStyle: UIAlertControllerStyle.Alert)
    
    // Add a way for the user to dismiss the alert via addAction().
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    
    // Present the alert on the screen by passing in the alert.
    self.presentViewController(alert, animated: true, completion: nil)
  }
}
