//
//  Slot.swift
//  SlotMachine
//
//  Created by Ross Castillo on 5/10/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

// NOTE:
// Prior to iOS 8, there were just 1x and 2x images, where 1x, or regular size, was for all iPhones
// without retina display, while 2x was for everything with retina display. This is because retina
// display has double the amount of pixels on the screen.
//
// However, with the iPhone 6 and 6+ there're 3x which is 3 times the original size of the image 
// which allows for a super sharp display.
//
// The original image is simply the "name", double sized is called "name@2x", and lastly, the triple
// sized images are called "name@3x".
//
// So, to actually add the images, just drag the folder containing the images into the sidebar and
// make sure the blue outline appears. Xcode will automatically detect the image asset size and file
// them in proper order.

import Foundation
import UIKit  // for UIImage

// Struct to encapsulate slot information. Don't need to make subclasses, so a struct is fine.
struct Slot {
  var value = 0
  var image = UIImage(named: "Ace")
  var isRed = true
}
