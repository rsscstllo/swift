//
//  Winnings.swift
//  SlotMachine
//
//  Created by Ross Castillo on 5/12/15.
//  Copyright (c) 2015 Ross Castillo. All rights reserved.
//

import Foundation

class Winnings {
  class func unpackSlotsIntoSlotRows(slots: [[Slot]]) -> [[Slot]] {
    // Create three arrays that hold slot instances to represent each *row* not column.
    var slotRow1: [Slot] = []
    var slotRow2: [Slot] = []
    var slotRow3: [Slot] = []
    
    // Fast enumeration to get all of the slot column arrays out of the slots double array.
    for slotArray in slots {
      // Iterate through each column array and add each slot to its appropriate row array.
      for var i = 0; i < slotArray.count; i++ {
        // Get the current slot.
        let slot = slotArray[i]
        
        if i == 0 {
          slotRow1.append(slot)
        } else if i == 1 {
          slotRow2.append(slot)
        } else if i == 2 {
          slotRow3.append(slot)
        } else {
          print("Error")
        }
      }
    }
    // Create an array of arrays that holds the slotRow arrays to be returned.
    let slotInRows: [[Slot]] = [slotRow1, slotRow2, slotRow3]
    return slotInRows
  }
  
  // Check and calculate winnings.
  class func computeWinnings(slots: [[Slot]]) -> Int {
    // Get slots in rows.
    let slotsInRows = unpackSlotsIntoSlotRows(slots)
    
    var winnings = 0
    
    // Three variables to keep track of the number of times of certain wins.
    var flushWinCount = 0
    var threeOfAKindWinCount = 0
    var straightWinCount = 0
    
    // Fast enumeration to iterate through each slot row array.
    for slotRow in slotsInRows {
      
      // Pass each slotRow array in to the checkFlush() function.
      if checkFlush(slotRow) == true {
        print("Flush")
        winnings += 1
        flushWinCount += 1
      }
      if flushWinCount == 3 {
        print("Royal Flush")
        winnings += 15
      }
      
      // Pass each slotRow array in to the checkStraight() function.
      if checkStraight(slotRow) {
        print("Three in a row")
        winnings += 1
        straightWinCount += 1
      }
      if straightWinCount == 3 {
        print("Epic straight")
        winnings += 1000
      }
      
      // Pass each slotRow array in to the checkThreeOfAKind() function.
      if checkThreeOfAKind(slotRow) {
        print("Three of a Kind")
        winnings += 3
        threeOfAKindWinCount += 1
      }
      if threeOfAKindWinCount == 3 {
        print("Threes all around")
        winnings += 50
      }
    }
    return winnings
  }
  
  // Check if all of the slots are the same color.
  class func checkFlush(slotRow: [Slot]) -> Bool {
    // Hard-coded. Could use for loop in future if more slots.
    let slot1 = slotRow[0]
    let slot2 = slotRow[1]
    let slot3 = slotRow[2]
    
    if slot1.isRed == true && slot2.isRed == true && slot3.isRed == true {
      return true
      
    } else if slot1.isRed == false && slot2.isRed == false && slot3.isRed == false {
      return true
      
    } else {
      return false
    }
  }
  
  // Check if the slots increase or decrease consecutively by 1.
  class func checkStraight(slotRow: [Slot]) -> Bool {
    // Get each slot instance.
    let slot1 = slotRow[0]
    let slot2 = slotRow[1]
    let slot3 = slotRow[2]
    
    if slot1.value == slot2.value - 1 && slot1.value == slot3.value - 2 {
      return true
    
    } else if slot1.value == slot2.value + 1 && slot1.value == slot3.value + 2{
      return true
    
    } else {
      return false
    }
  }
  
  // Check if each slot value is the same.
  class func checkThreeOfAKind (slotRow: [Slot]) -> Bool {
    // Get each slot instance.
    let slot1 = slotRow[0]
    let slot2 = slotRow[1]
    let slot3 = slotRow[2]
    
    if slot1.value == slot2.value && slot1.value == slot3.value {
      return true
    } else {
      return false
    }
  }
}
